# Project Echo
A forge 1.12.2 modpack

---
---

## Client Setup
* First download and install the recommended version of [forge 1.12.2](https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html).
* Download all mods [here](https://gitlab.com/alwus/project-echo/-/archive/master/project-echo-master.zip) and extract the mods directly into your mods folder in your .minecraft folder.
* Select the forge 1.12.2 profile in the Minecraft launcher and start the game.

## Server Setup
For setting up your own server, it is recommended to use docker with the latest [itzg/minecraft-server](https://hub.docker.com/r/itzg/minecraft-server) image.  
Also download the the recommended version of [forge 1.12.2](https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html) and put the .jar installer in your server folder.  
In your environmentals for the docker, set VERSION to 1.12.2, MEMORY to at least 4G, TYPE to FORGE and FORGE_INSTALLER to the name of the installer .jar file.

Before starting the server, don't forget to set level-type in server.properties to BIOMESOP.
